<?php

namespace frontend\assets;

use yii\web\AssetBundle;

class WebVendorAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
       'vendor/bower/select2/dist/css/select2.min.css',
    ];
    public $js = [
   'vendor/bower/select2/dist/js/select2.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
