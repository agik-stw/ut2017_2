<?php

namespace frontend\assets;

use yii\web\AssetBundle;

class PluginsAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
       'plugins/StickyTableHeaders-master/css/custom.css',
       'plugins/StickyTableHeaders-master/css/tablesorter.css',
    ];
    public $js = [
   'plugins/StickyTableHeaders-master/js/jquery.stickytableheaders.min.js',
   'plugins/StickyTableHeaders-master/js/jquery.tablesorter.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
