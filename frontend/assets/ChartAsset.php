<?php

namespace frontend\assets;

use yii\web\AssetBundle;

class ChartAsset extends AssetBundle
{
   public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
       'vendor/bower/c3/c3.min.css',
    ];
    public $js = [
   'vendor/bower/d3/d3.min.js',
   'vendor/bower/c3/c3.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
