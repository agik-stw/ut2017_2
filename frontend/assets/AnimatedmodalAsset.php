<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AnimatedmodalAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'vendor/bower/animatedmodal/demo/css/normalize.min.css',
        'vendor/bower/animatedmodal/demo/css/animate.min.css',
    ];
    public $js = [
    'vendor/bower/animatedmodal/demo/js/animatedModal.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
