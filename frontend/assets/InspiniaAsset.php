<?php

namespace frontend\assets;

use yii\web\AssetBundle;

class InspiniaAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
       'inspinia/font-awesome/css/font-awesome.css',
       'inspinia/css/plugins/toastr/toastr.min.css',
       'inspinia/js/plugins/gritter/jquery.gritter.css',
       'inspinia/css/plugins/datapicker/datepicker3.css',
       'inspinia/css/plugins/daterangepicker/daterangepicker-bs3.css',
       'inspinia/css/animate.css',
       'inspinia/css/style.css',
    ];
    public $js = [
   'inspinia/js/plugins/metisMenu/jquery.metisMenu.js',
   'inspinia/js/plugins/slimscroll/jquery.slimscroll.min.js',
   'inspinia/js/plugins/flot/jquery.flot.js',
   'inspinia/js/plugins/flot/jquery.flot.tooltip.min.js',
   'inspinia/js/plugins/flot/jquery.flot.spline.js',
   'inspinia/js/plugins/flot/jquery.flot.resize.js',
   'inspinia/js/plugins/flot/jquery.flot.pie.js',
   'inspinia/js/plugins/peity/jquery.peity.min.js',
   'inspinia/js/demo/peity-demo.js',
   'inspinia/js/inspinia.js',
   'inspinia/js/plugins/pace/pace.min.js',
   'inspinia/js/plugins/jquery-ui/jquery-ui.min.js',
   'inspinia/js/plugins/gritter/jquery.gritter.min.js',
   'inspinia/js/plugins/sparkline/jquery.sparkline.min.js',
   'inspinia/js/demo/sparkline-demo.js',
   'inspinia/js/plugins/chartJs/Chart.min.js',
   'inspinia/js/plugins/toastr/toastr.min.js',
   'inspinia/js/plugins/datapicker/bootstrap-datepicker.js',
   'inspinia/js/plugins/daterangepicker/daterangepicker.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
