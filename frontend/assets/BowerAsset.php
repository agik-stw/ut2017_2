<?php

namespace frontend\assets;

use yii\web\AssetBundle;

class BowerAsset extends AssetBundle
{
   public $sourcePath = '@bower';
    public $css = [
        'datatables.net-dt/css/jquery.dataTables.min.css',
        'datatables.net-buttons-dt/css/buttons.dataTables.min.css',
/*        'datatables.net-responsive-dt/css/responsive.dataTables.min.css',
        'datatables.net-fixedheader-dt/css/fixedheader.dataTables.min.css',*/
        'datatables.net-select-dt/css/select.dataTables.min.css',
        /*'select2/dist/css/select2.css',*/
    ];
    public $js = [
    'datatables.net/js/jquery.dataTables.min.js',
    'datatables.net-buttons/js/dataTables.buttons.min.js',
/*    'datatables.net-responsive/js/dataTables.responsive.min.js',
    'datatables.net-fixedheader/js/dataTables.fixedheader.min.js',*/
    'datatables.net-select/js/dataTables.select.min.js',
    /*'select2/dist/js/select2.js',*/
    'bower-angularjs/angular.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
