<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_transaction".
 *
 * @property string $grouploc
 * @property string $lab_no
 * @property int $componentid
 * @property int $customer_id
 * @property string $name
 * @property string $address
 * @property string $branch
 * @property int $unit_id
 * @property string $unit_no
 * @property string $model
 * @property string $component
 * @property string $oil_type
 * @property string $oil_matrix
 * @property string $orig_visc
 * @property string $sampl_dt1
 * @property string $recv_dt1
 * @property string $rpt_dt1
 * @property string $hrs_km_oh
 * @property string $hrs_km_oc
 * @property string $hrs_km_tot
 * @property string $oil_change
 * @property string $reff
 * @property double $zddp
 * @property string $pccek
 * @property double $visc_cst
 * @property string $cst_code
 * @property int $visc_index
 * @property string $visc_index_code
 * @property double $visc_sae
 * @property string $sae_code
 * @property double $dilution
 * @property string $dilut_code
 * @property double $dir_trans
 * @property string $trans_code
 * @property double $oxidation
 * @property string $oxid_code
 * @property double $nitration
 * @property string $nitr_code
 * @property double $water
 * @property string $wtr_code
 * @property double $t_a_n
 * @property string $tan_code
 * @property double $t_b_n
 * @property string $tbn_code
 * @property string $add_phys
 * @property string $add_value
 * @property string $add_code
 * @property double $nickel
 * @property string $ni_code
 * @property double $sox
 * @property string $sox_code
 * @property double $glycol
 * @property string $gly_code
 * @property double $silicon
 * @property string $si_code
 * @property double $iron
 * @property double $iron_conv
 * @property string $fe_code
 * @property double $copper
 * @property double $copper_conv
 * @property string $cu_code
 * @property double $aluminium
 * @property double $alumunium_conv
 * @property string $al_code
 * @property double $chromium
 * @property double $chromium_conv
 * @property string $cr_code
 * @property double $magnesium
 * @property string $mg_code
 * @property double $silver
 * @property string $ag_code
 * @property double $tin
 * @property string $sn_code
 * @property double $lead
 * @property double $lead_conv
 * @property string $pb_code
 * @property double $sodium
 * @property double $sodium_conv
 * @property string $na_code
 * @property double $calcium
 * @property string $ca_code
 * @property double $zinc
 * @property string $zn_code
 * @property string $add_met
 * @property double $add_mval
 * @property string $add_mcode
 * @property string $eval_code
 * @property string $ut_emd
 * @property string $ut_esn
 * @property string $recomm1
 * @property string $recomm2
 * @property string $recomm3
 * @property string $recomm4
 * @property string $matrix
 * @property double $molybdenum
 * @property string $molybdenum_code
 * @property double $boron
 * @property string $boron_code
 * @property double $potassium
 * @property string $potassium_code
 * @property double $barium
 * @property string $barium_code
 * @property double $4um
 * @property string $4um_code
 * @property double $6um
 * @property string $6um_code
 * @property double $15um
 * @property string $15um_code
 * @property string $iso4406
 * @property string $iso4406_code
 * @property double $pqindex
 * @property string $pqindex_code
 * @property string $colourcode
 * @property string $colourcode_code
 * @property double $phosphor
 * @property string $phosphor_code
 * @property double $sulphur
 * @property double $visc_40
 * @property string $visc_40_code
 * @property string $seq_i
 * @property string $seq_i_code
 * @property string $seq_ii
 * @property string $seq_ii_code
 * @property string $seq_iii
 * @property string $seq_iii_code
 * @property string $filter_code
 * @property string $filter_desc
 * @property string $magnetic_code
 * @property string $magnetic_desc
 * @property double $karlfischer
 * @property double $rubbingwearsize
 * @property string $rubbingwearconc
 * @property double $cuttingwearsize
 * @property string $cuttingwearconc
 * @property double $scuffingwearsize
 * @property string $scuffingwearconc
 * @property double $fatiguewearsize
 * @property string $fatiguewearconc
 * @property double $fatiguelaminarsize
 * @property string $fatiguelaminarconc
 * @property double $spheressize
 * @property string $spheresconc
 * @property double $darkoxidessize
 * @property string $darkoxidesconc
 * @property double $redoxidessize
 * @property string $redoxidesconc
 * @property double $nonferroussize
 * @property string $nonferrousconc
 * @property double $miscdustsize
 * @property string $miscdustconc
 * @property string $notes
 */
class TbTransaction extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_transaction';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['lab_no', 'componentid', 'name', 'branch', 'unit_id'], 'required'],
            [['componentid', 'customer_id', 'unit_id', 'visc_index'], 'integer'],
            [['orig_visc', 'hrs_km_oh', 'hrs_km_oc', 'hrs_km_tot', 'zddp', 'visc_cst', 'visc_sae', 'dilution', 'dir_trans', 'oxidation', 'nitration', 'water', 't_a_n', 't_b_n', 'nickel', 'sox', 'glycol', 'silicon', 'iron', 'iron_conv', 'copper', 'copper_conv', 'aluminium', 'alumunium_conv', 'chromium', 'chromium_conv', 'magnesium', 'silver', 'tin', 'lead', 'lead_conv', 'sodium', 'sodium_conv', 'calcium', 'zinc', 'add_mval', 'molybdenum', 'boron', 'potassium', 'barium', '4um', '6um', '15um', 'pqindex', 'phosphor', 'sulphur', 'visc_40', 'karlfischer', 'rubbingwearsize', 'cuttingwearsize', 'scuffingwearsize', 'fatiguewearsize', 'fatiguelaminarsize', 'spheressize', 'darkoxidessize', 'redoxidessize', 'nonferroussize', 'miscdustsize'], 'number'],
            [['sampl_dt1', 'recv_dt1', 'rpt_dt1'], 'safe'],
            [['recomm1', 'recomm2', 'recomm3', 'recomm4', 'phosphor_code'], 'string'],
            [['grouploc', 'add_phys', 'add_value', 'add_code', 'add_met', 'ut_emd', 'ut_esn', 'matrix'], 'string', 'max' => 15],
            [['lab_no', 'iso4406', 'colourcode', 'seq_i', 'seq_ii', 'seq_iii', 'rubbingwearconc', 'cuttingwearconc', 'scuffingwearconc', 'fatiguewearconc', 'fatiguelaminarconc', 'spheresconc', 'darkoxidesconc', 'redoxidesconc', 'nonferrousconc', 'miscdustconc'], 'string', 'max' => 10],
            [['name', 'address', 'branch'], 'string', 'max' => 50],
            [['unit_no', 'model', 'oil_type'], 'string', 'max' => 25],
            [['component', 'reff'], 'string', 'max' => 30],
            [['oil_matrix', 'sae_code'], 'string', 'max' => 20],
            [['oil_change'], 'string', 'max' => 3],
            [['pccek'], 'string', 'max' => 5],
            [['cst_code', 'visc_40_code'], 'string', 'max' => 2],
            [['visc_index_code', 'dilut_code', 'trans_code', 'oxid_code', 'nitr_code', 'wtr_code', 'tan_code', 'tbn_code', 'ni_code', 'sox_code', 'gly_code', 'si_code', 'fe_code', 'cu_code', 'al_code', 'cr_code', 'mg_code', 'ag_code', 'sn_code', 'pb_code', 'na_code', 'ca_code', 'zn_code', 'add_mcode', 'eval_code', 'molybdenum_code', 'potassium_code', 'barium_code', '4um_code', '6um_code', '15um_code', 'iso4406_code', 'pqindex_code', 'colourcode_code', 'seq_i_code', 'seq_ii_code', 'seq_iii_code'], 'string', 'max' => 1],
            [['boron_code'], 'string', 'max' => 8],
            [['filter_code', 'magnetic_code'], 'string', 'max' => 100],
            [['filter_desc', 'magnetic_desc', 'notes'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'grouploc' => 'Grouploc',
            'lab_no' => 'Lab No',
            'componentid' => 'Componentid',
            'customer_id' => 'Customer ID',
            'name' => 'Name',
            'address' => 'Address',
            'branch' => 'Branch',
            'unit_id' => 'Unit ID',
            'unit_no' => 'Unit No',
            'model' => 'Model',
            'component' => 'Component',
            'oil_type' => 'Oil Type',
            'oil_matrix' => 'Oil Matrix',
            'orig_visc' => 'Orig Visc',
            'sampl_dt1' => 'Sampl Dt1',
            'recv_dt1' => 'Recv Dt1',
            'rpt_dt1' => 'Rpt Dt1',
            'hrs_km_oh' => 'Hrs Km Oh',
            'hrs_km_oc' => 'Hrs Km Oc',
            'hrs_km_tot' => 'Hrs Km Tot',
            'oil_change' => 'Oil Change',
            'reff' => 'Reff',
            'zddp' => 'Zddp',
            'pccek' => 'Pccek',
            'visc_cst' => 'Visc Cst',
            'cst_code' => 'Cst Code',
            'visc_index' => 'Visc Index',
            'visc_index_code' => 'Visc Index Code',
            'visc_sae' => 'Visc Sae',
            'sae_code' => 'Sae Code',
            'dilution' => 'Dilution',
            'dilut_code' => 'Dilut Code',
            'dir_trans' => 'Dir Trans',
            'trans_code' => 'Trans Code',
            'oxidation' => 'Oxidation',
            'oxid_code' => 'Oxid Code',
            'nitration' => 'Nitration',
            'nitr_code' => 'Nitr Code',
            'water' => 'Water',
            'wtr_code' => 'Wtr Code',
            't_a_n' => 'T A N',
            'tan_code' => 'Tan Code',
            't_b_n' => 'T B N',
            'tbn_code' => 'Tbn Code',
            'add_phys' => 'Add Phys',
            'add_value' => 'Add Value',
            'add_code' => 'Add Code',
            'nickel' => 'Nickel',
            'ni_code' => 'Ni Code',
            'sox' => 'Sox',
            'sox_code' => 'Sox Code',
            'glycol' => 'Glycol',
            'gly_code' => 'Gly Code',
            'silicon' => 'Silicon',
            'si_code' => 'Si Code',
            'iron' => 'Iron',
            'iron_conv' => 'Iron Conv',
            'fe_code' => 'Fe Code',
            'copper' => 'Copper',
            'copper_conv' => 'Copper Conv',
            'cu_code' => 'Cu Code',
            'aluminium' => 'Aluminium',
            'alumunium_conv' => 'Alumunium Conv',
            'al_code' => 'Al Code',
            'chromium' => 'Chromium',
            'chromium_conv' => 'Chromium Conv',
            'cr_code' => 'Cr Code',
            'magnesium' => 'Magnesium',
            'mg_code' => 'Mg Code',
            'silver' => 'Silver',
            'ag_code' => 'Ag Code',
            'tin' => 'Tin',
            'sn_code' => 'Sn Code',
            'lead' => 'Lead',
            'lead_conv' => 'Lead Conv',
            'pb_code' => 'Pb Code',
            'sodium' => 'Sodium',
            'sodium_conv' => 'Sodium Conv',
            'na_code' => 'Na Code',
            'calcium' => 'Calcium',
            'ca_code' => 'Ca Code',
            'zinc' => 'Zinc',
            'zn_code' => 'Zn Code',
            'add_met' => 'Add Met',
            'add_mval' => 'Add Mval',
            'add_mcode' => 'Add Mcode',
            'eval_code' => 'Eval Code',
            'ut_emd' => 'Ut Emd',
            'ut_esn' => 'Ut Esn',
            'recomm1' => 'Recomm1',
            'recomm2' => 'Recomm2',
            'recomm3' => 'Recomm3',
            'recomm4' => 'Recomm4',
            'matrix' => 'Matrix',
            'molybdenum' => 'Molybdenum',
            'molybdenum_code' => 'Molybdenum Code',
            'boron' => 'Boron',
            'boron_code' => 'Boron Code',
            'potassium' => 'Potassium',
            'potassium_code' => 'Potassium Code',
            'barium' => 'Barium',
            'barium_code' => 'Barium Code',
            '4um' => '4um',
            '4um_code' => '4um Code',
            '6um' => '6um',
            '6um_code' => '6um Code',
            '15um' => '15um',
            '15um_code' => '15um Code',
            'iso4406' => 'Iso4406',
            'iso4406_code' => 'Iso4406 Code',
            'pqindex' => 'Pqindex',
            'pqindex_code' => 'Pqindex Code',
            'colourcode' => 'Colourcode',
            'colourcode_code' => 'Colourcode Code',
            'phosphor' => 'Phosphor',
            'phosphor_code' => 'Phosphor Code',
            'sulphur' => 'Sulphur',
            'visc_40' => 'Visc 40',
            'visc_40_code' => 'Visc 40 Code',
            'seq_i' => 'Seq I',
            'seq_i_code' => 'Seq I Code',
            'seq_ii' => 'Seq Ii',
            'seq_ii_code' => 'Seq Ii Code',
            'seq_iii' => 'Seq Iii',
            'seq_iii_code' => 'Seq Iii Code',
            'filter_code' => 'Filter Code',
            'filter_desc' => 'Filter Desc',
            'magnetic_code' => 'Magnetic Code',
            'magnetic_desc' => 'Magnetic Desc',
            'karlfischer' => 'Karlfischer',
            'rubbingwearsize' => 'Rubbingwearsize',
            'rubbingwearconc' => 'Rubbingwearconc',
            'cuttingwearsize' => 'Cuttingwearsize',
            'cuttingwearconc' => 'Cuttingwearconc',
            'scuffingwearsize' => 'Scuffingwearsize',
            'scuffingwearconc' => 'Scuffingwearconc',
            'fatiguewearsize' => 'Fatiguewearsize',
            'fatiguewearconc' => 'Fatiguewearconc',
            'fatiguelaminarsize' => 'Fatiguelaminarsize',
            'fatiguelaminarconc' => 'Fatiguelaminarconc',
            'spheressize' => 'Spheressize',
            'spheresconc' => 'Spheresconc',
            'darkoxidessize' => 'Darkoxidessize',
            'darkoxidesconc' => 'Darkoxidesconc',
            'redoxidessize' => 'Redoxidessize',
            'redoxidesconc' => 'Redoxidesconc',
            'nonferroussize' => 'Nonferroussize',
            'nonferrousconc' => 'Nonferrousconc',
            'miscdustsize' => 'Miscdustsize',
            'miscdustconc' => 'Miscdustconc',
            'notes' => 'Notes',
        ];
    }
}
