 <?php
use yii\helpers\Url;
$this->title = 'Monitoring | Fuel';
use richardfan\widget\JSRegister;
?>
<div class="ibox-title">
<div class="col-md-2 pull-left">
<div class="form-group">
  <label for="export">Option Export:</label>
  <select disabled="" class="form-control input-sm select2" id="export">
  <option disabled="" selected="" value="Select_Export">Select Export</option>
    <option value="pdf">PDF</option>
    <option value="excel">EXCEL</option>
  </select>
</div>
</div>
                <table style="width: 100%;" id="tb_used_oil" class="table table-striped table-hover" >
                    <thead>
                        <tr>
                            <th width="80" class="th_table">Group</th>
                            <th width="80" class="th_table">Branch</th>
                            <th width="150" class="th_table">Customer Name</th>
                            <th width="150" class="th_table">Lab Number</th>
                            <th width="150" class="th_table">Sample Date</th>
                            <th width="150" class="th_table">Receive Date</th>
                            <th width="150" class="th_table">Report Date</th>
                            <th width="150" class="th_table">Unit Number</th>
                            <th class="th_table">Component</th>
                            <th class="th_table">Model</th>
                          <!--   <th class="th_table">Serial No.</th>
                            <th class="th_table">Oil Chg</th>
                            <th class="th_table">Comp Chg</th>
                            <th class="th_table">FC</th>
                            <th class="th_table">MP</th> -->
                            <th class="th_table">Status</th>
                        </tr>
                    </thead>
                    <tbody>

                           
                                    </tbody>
                                </table>
                                </div>
            <div style="margin-top: 50px;"></div>

            <input type="hidden" name="labno" id="labno">

<?php JSRegister::begin(); ?>
<script>
var tb=$("#tb_used_oil").DataTable({
     "paging": true,
     "bSort": false,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "responsive": true,
        "autoWidth":true,
        "ajax": {
          "url": "<?php echo Url::toRoute('/api/fuel/getdata?token=itpetrolab');?>",
          'type':'POST'
        },
    columns: [
    {data:'grouploc'},
    {data:'branch'},
    {data:'customer_name'},
    {data:'lab_no'},
    {data:'sample_date'},
    {data:'receive_date'},
    {data:'report_date'},
    {data:'unit_number'},
    {data:'component_name'},
    {data:'model'},
    /*{data:'lab_no'},
    {data:'oil_change'},
    {data:'cmp'},
    {data:'filter_code'},
    {data:'mp'},*/
    {data:'eval_code'}
    ],
    scrollY:'63vh',
    scrollCollapse: true,
        "scrollX": true,
        processing: true,
        serverSide: false,
        select: {
            style: 'single'
        },
        select: true,
 "rowCallback": function( row, data, index ) {
        /*alert(data.eval_code);*/
 if (data.eval_code=='Urgent') {
/*$('td', row).css('background-color', '#ff6666');
$('td', row).css('color', 'white');*/
 $(row).find('td:eq(15)').css('background-color', '#ff6666');
 }else if(data.eval_code=='Attention'){
/*   $('td', row).css('background-color', '#ffa31a'); 
   $('td', row).css('color', 'white');*/ 
   $(row).find('td:eq(15)').css('background-color', '#ffa31a');
 }else if(data.eval_code=='Normal'){
/*   $('td', row).css('background-color', '#ffa31a'); 
   $('td', row).css('color', 'white');*/ 
   $(row).find('td:eq(15)').css('background-color', '#b3ff66');
 }else if(data.eval_code==' '){
/*   $('td', row).css('background-color', '#ffa31a'); 
   $('td', row).css('color', 'white');*/ 
   $(row).find('td:eq(15)').css('background-color', '#b3ff66');
 }

}
 
});

$('#tb_used_oil tbody').on( 'click', 'tr', function () {
var data=( tb.row( this ).data() );
/*alert(data.lab_no);*/
$("#labno").val(data.lab_no);
$("#export").removeAttr('disabled');
ymz.jq_toast({text:"Lab Number: "+data.lab_no+"", type: "notice", sec: 5});
});

$(".select2").select2();

$("#tb_used_oil td").each(function() {
    if ($(this).text() == 'N') {
      $(this).css('color', 'red');
    }
  });

$("#export").on('change',function(event) {
var type=$(this).val();
var labno=$("#labno").val();
window.open("<?php echo Url::toRoute('/monitoring/fuel/action/report?type=');?>"+type+'&'+'labNumber='+labno,'_blank')
$("#export").val('Select_Export');
});

$("#tb_used_oil").css({"width":"100%"});
</script>
<?php JSRegister::end(); ?>



<?php

$this->registerJsFile(
    '@web/modulejs/usedOil/controller.js',
    ['depends' => [\yii\web\JqueryAsset::className()]]
);

?>